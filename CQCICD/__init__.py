from __future__ import absolute_import, unicode_literals

# This will make sure the app is always imported when
# Django starts so that shared_task will use this app.
from .celery import app as celery_app
import os

__all__ = ['celery_app', 'django_env']


def django_env():
    if os.environ.get('DJANGO_ENV') == 'DEBUG':
        return True
    return False
