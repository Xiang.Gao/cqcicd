#!/bin/sh

if [ `uname` = "Linux" ]; then
    pid_file="/var/run/cicd.pid"
    sock_file="/var/run/cicd.sock"
else
    pid_file="/usr/local/var/run/cicd.pid"
    sock_file="/usr/local/var/run/cicd.sock"
fi

check_pid(){
    if [ -f $pid_file ];then
        echo "gunicorn started!"
    else
        echo "gunicorn stopped!"
    fi
}

start(){
    if [ -f $pid_file ]; then
        echo "gunicorn is running in currently!"
    else
        cd `dirname $0`
        gunicorn CQCICD.wsgi -w 2 -p $pid_file -b unix:$sock_file -D
        sleep 1
        check_pid
    fi
}

stop(){
    if [ ! -f $pid_file ]; then
        echo "gunicorn not running!"
    else
        # Graceful shutdown. Waits for workers to finish their current requests up to the graceful_timeout.
        kill -TERM `cat $pid_file`
        sleep 1
        check_pid
    fi
}

restart(){
    if [ -f $pid_file ]; then
        # gracefully reload by sending HUP signal to gunicorn
        kill -HUP `cat $pid_file`
    else
        echo "gunicorn not running in background, starting ..."
        start
    fi
}

celery_start(){
    cd `dirname $0`
    /usr/local/bin/celery multi start celery -A CQCICD worker -l info --logfile=/var/log/celery.log --pidfile=/var/run/%n.pid
}

celery_stop(){
    cd `dirname $0`
    /usr/local/bin/celery multi stop celery -A CQCICD worker -l info --logfile=/var/log/celery.log --pidfile=/var/run/%n.pid
}

celery_restart(){
    cd `dirname $0`
    /usr/local/bin/celery multi restart celery -A CQCICD worker -l info --logfile=/var/log/celery.log --pidfile=/var/run/%n.pid
}

case $1 in 
    start)
        start
    ;;
    stop)
        stop
    ;;
    restart)
        restart
    ;;
    celery_start)
        celery_start
    ;;
    celery_stop)
        celery_stop
    ;;
    celery_restart)
        celery_restart
    ;;

esac
