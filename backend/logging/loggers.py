# -*- coding:utf-8 -*-
# Author:Gao Xiang

import logging
import logging.config
from CQCICD.settings import LOGGING


def loggers():
    logging.config.dictConfig(LOGGING)
    logger = logging.getLogger('CQCICD.custom')
    return logger
