
# Create your views here.
from django.shortcuts import redirect
from django.views.generic.base import TemplateView
from CQCICD.settings import LOGGING
from django.contrib.auth import authenticate, login
import logging.config


logging.config.dictConfig(LOGGING)
logger = logging.getLogger('cqcicd.cicd')


class Login(TemplateView):
    template_name = 'login.html'

    def get_context_data(self, **kwargs):
        context = {
            'next': self.request.GET.get('next', '/')
        }
        kwargs.update(context)
        return super().get_context_data(**kwargs)

    def post(self, request):
        redirect_url = request.GET.get('next')  # 获取POST提交url参数
        username = request.POST.get('username')
        password = request.POST.get('password')
        user = authenticate(request, name=username, password=password)
        if user:
            login(request, user)
            request.session.update({'username': username})
            if request.POST.get('rmb') == 'on':
                request.session.set_expiry(60 * 60 * 24 * 30)
            return redirect(redirect_url)
        return redirect('/backend/login/')

