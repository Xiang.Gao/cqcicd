
# import collections
# from random import choice
#
# Card = collections.namedtuple('Card', ['rank', 'suit'])
#
#
# class FrenchDeck:
#     ranks = [str(n) for n in range(2, 11)] + list('JQKA')
#     suit = 'spades diamonds clubs hearts'.split()
#
#     def __init__(self):
#         self._cards = [Card(rank, suit) for suit in self.suit for rank in self.ranks]
#
#     def __len__(self):
#         return len(self._cards)
#
#     def __getitem__(self, position):
#         return self._cards[position]
#
#
# suit_values = dict(spades=3, hearts=2, diamonds=1, clubs=0)
#
#
# def spades_high(card):
#     rank_value = FrenchDeck.ranks.index(card.rank)
#     print(rank_value)
#     return rank_value * len(suit_values) + suit_values[card.suit]
#
#
# deck = FrenchDeck()
#
# for card in sorted(deck, key=spades_high):
#     print(card)

import os
from CQCICD.settings import BASE_DIR

#path = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
path = os.path.join(os.sep, 'etc')
print(path)



