# -*- coding:utf-8 -*-
# Author:Gao Xiang

from django.shortcuts import redirect


def login_required(func):
    def wrapper(request, *args, **wargs):
        if request.session.session_key:
            return func(request, *args, **wargs)
        return redirect('/backend/login/')
    return wrapper