# -*- coding: utf-8 -*-
# Generated by Django 1.11.4 on 2018-05-23 07:41
from __future__ import unicode_literals

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('backend', '0005_auto_20180523_1442'),
    ]

    operations = [
        migrations.AlterField(
            model_name='disks',
            name='InstanceId',
            field=models.ForeignKey(null=True, on_delete=django.db.models.deletion.CASCADE, to='backend.Ecses'),
        ),
        migrations.AlterField(
            model_name='disks',
            name='MountInstances',
            field=models.CharField(max_length=64, null=True),
        ),
    ]
