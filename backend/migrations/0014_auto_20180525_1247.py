# -*- coding: utf-8 -*-
# Generated by Django 1.11.4 on 2018-05-25 04:47
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('backend', '0013_auto_20180525_1204'),
    ]

    operations = [
        migrations.AlterField(
            model_name='ecses',
            name='InstanceName',
            field=models.CharField(max_length=64),
        ),
    ]
