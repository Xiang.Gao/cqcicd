CQASOWEB = {
        'event_name': 'push',
        'ref': 'refs/heads/alpha',
        'repository': {
            'homepage': 'https://gitlab.com/Xiang.Gao/cqaso-web',
            'git_ssh_url': 'git@gitlab.com:Xiang.Gao/cqaso-web.git',
            'description': 'cqaso web',
            'git_http_url': 'https://gitlab.com/Xiang.Gao/cqaso-web.git',
            'url': 'git@gitlab.com:Xiang.Gao/cqaso-web.git',
            'name': 'cqaso-web',
            'visibility_level': 0
        },
        'user_email': 'gaoxiang@chuangqish.com',
        'user_name': "gao's",
        'user_username': 'Xiang.Gao',
        'total_commits_count': 3,
        'after': '7535e798cedd3aa785ac1b20e57ed4e5d3f7c52a',
        'commits': [{
            'modified': ['src/layout/Navbar.jsx'],
            'timestamp': '2018-11-09T03:42:05Z',
            'removed': [],
            'id': '7535e798cedd3aa785ac1b20e57ed4e5d3f7c52a',
            'url': 'https://gitlab.com/Xiang.Gao/cqaso-web/commit/7535e798cedd3aa785ac1b20e57ed4e5d3f7c52a',
            'message': '[IMP]解决冲突\n',
            'added': [],
            'author': {
                'email': 'raion@raiondeMacBook-Pro.local',
                'name': 'raion'
            }
        }, {
            'modified': ['src/component/HistoryApps.jsx', 'src/layout/Navbar.jsx'],
            'timestamp': '2018-11-09T01:13:43Z',
            'removed': [],
            'id': '9a982245357862b97c4bcd4749fbc5758692255d',
            'url': 'https://gitlab.com/Xiang.Gao/cqaso-web/commit/9a982245357862b97c4bcd4749fbc5758692255d',
            'message': '[IMP]锦鲤活动规则、样式修改\n',
            'added': [],
            'author': {
                'email': 'chenqianghao@chuangqish.com',
                'name': 'chenqianghao'
            }
        }, {
            'modified': ['src/component/HistoryApps.jsx', 'src/component/LoginWindow.jsx', 'src/page/Home/index.js', 'src/page/toplist/api.js', 'src/page/toplist/component/Navbar.jsx'],
            'timestamp': '2018-11-08T08:57:01Z',
            'removed': [],
            'id': '8639224ec812e6162a3bcba98086b621d510a09d',
            'url': 'https://gitlab.com/Xiang.Gao/cqaso-web/commit/8639224ec812e6162a3bcba98086b621d510a09d',
            'message': '[IMP]解决冲突\n',
            'added': [],
            'author': {
                'email': 'raion@raiondeMacBook-Pro.local',
                'name': 'raion'
            }
        }],
        'user_avatar': 'https://secure.gravatar.com/avatar/c600618eb135e969cf14758d6a07225c?s=80&d=identicon',
        'project': {
            'homepage': 'https://gitlab.com/Xiang.Gao/cqaso-web',
            'ci_config_path': None,
            'web_url': 'https://gitlab.com/Xiang.Gao/cqaso-web',
            'id': 9291225,
            'default_branch': 'alpha',
            'namespace': 'Xiang.Gao',
            'ssh_url': 'git@gitlab.com:Xiang.Gao/cqaso-web.git',
            'git_ssh_url': 'git@gitlab.com:Xiang.Gao/cqaso-web.git',
            'name': 'cqaso-web',
            'description': 'cqaso web',
            'path_with_namespace': 'Xiang.Gao/cqaso-web',
            'http_url': 'https://gitlab.com/Xiang.Gao/cqaso-web.git',
            'git_http_url': 'https://gitlab.com/Xiang.Gao/cqaso-web.git',
            'url': 'git@gitlab.com:Xiang.Gao/cqaso-web.git',
            'avatar_url': None,
            'visibility_level': 0
        },
        'before': '8639224ec812e6162a3bcba98086b621d510a09d',
        'object_kind': 'push',
        'project_id': 9291225,
        'user_id': 889778,
        'push_options': [],
        'checkout_sha': '7535e798cedd3aa785ac1b20e57ed4e5d3f7c52a',
        'message': None
}

CQASOBACKEND = {
        'event_name': 'push',
        'user_email': 'gaoxiang@chuangqish.com',
        'object_kind': 'push',
        'user_avatar': 'https://secure.gravatar.com/avatar/c600618eb135e969cf14758d6a07225c?s=80&d=identicon',
        'commits': [{
            'timestamp': '2018-11-09T02:21:56Z',
            'id': 'ea6cc9f8cf2113c057ac8ffbb923f9f9102c8a69',
            'added': [],
            'modified': ['cqaso-consumer/src/main/java/com/cqaso/consumer/service/AppConsumerService.java'],
            'removed': [],
            'url': 'https://gitlab.com/Xiang.Gao/cqaso-backend/commit/ea6cc9f8cf2113c057ac8ffbb923f9f9102c8a69',
            'message': 'opt\n',
            'author': {
                'name': 'huishen',
                'email': 'darkaquarius88@163.com'
            }
        }, {
            'timestamp': '2018-11-09T02:15:49Z',
            'id': '3ffa877ae698fc142775d645134d3fff3ddc26b3',
            'added': [],
            'modified': ['cqaso-api/src/main/java/com/cqaso/Task.java'],
            'removed': [],
            'url': 'https://gitlab.com/Xiang.Gao/cqaso-backend/commit/3ffa877ae698fc142775d645134d3fff3ddc26b3',
            'message': 'opt\n',
            'author': {
                'name': 'zhangjessey',
                'email': ''
            }
        }, {
            'timestamp': '2018-11-08T11:56:06Z',
            'id': '817f38ef6360eb8ad084dccd868129b1201115dd',
            'added': [],
            'modified': ['cqaso-api/src/main/java/com/cqaso/service/AppService.java'],
            'removed': [],
            'url': 'https://gitlab.com/Xiang.Gao/cqaso-backend/commit/817f38ef6360eb8ad084dccd868129b1201115dd',
            'message': 'opt 分词\n',
            'author': {
                'name': 'huishen',
                'email': 'darkaquarius88@163.com'
            }
        }],
        'project_id': 9289193,
        'user_username': 'Xiang.Gao',
        'user_id': 889778,
        'checkout_sha': 'ea6cc9f8cf2113c057ac8ffbb923f9f9102c8a69',
        'ref': 'refs/heads/dev',
        'project': {
            'default_branch': 'dev',
            'homepage': 'https://gitlab.com/Xiang.Gao/cqaso-backend',
            'web_url': 'https://gitlab.com/Xiang.Gao/cqaso-backend',
            'id': 9289193,
            'path_with_namespace': 'Xiang.Gao/cqaso-backend',
            'description': 'cqaso backends.',
            'git_ssh_url': 'git@gitlab.com:Xiang.Gao/cqaso-backend.git',
            'ci_config_path': None,
            'url': 'git@gitlab.com:Xiang.Gao/cqaso-backend.git',
            'visibility_level': 0,
            'avatar_url': None,
            'http_url': 'https://gitlab.com/Xiang.Gao/cqaso-backend.git',
            'name': 'cqaso-backend',
            'ssh_url': 'git@gitlab.com:Xiang.Gao/cqaso-backend.git',
            'git_http_url': 'https://gitlab.com/Xiang.Gao/cqaso-backend.git',
            'namespace': 'Xiang.Gao'
        },
        'total_commits_count': 3,
        'after': 'ea6cc9f8cf2113c057ac8ffbb923f9f9102c8a69',
        'before': '817f38ef6360eb8ad084dccd868129b1201115dd',
        'repository': {
            'homepage': 'https://gitlab.com/Xiang.Gao/cqaso-backend',
            'description': 'cqaso backends.',
            'name': 'cqaso-backend',
            'git_ssh_url': 'git@gitlab.com:Xiang.Gao/cqaso-backend.git',
            'url': 'git@gitlab.com:Xiang.Gao/cqaso-backend.git',
            'git_http_url': 'https://gitlab.com/Xiang.Gao/cqaso-backend.git',
            'visibility_level': 0
        },
        'push_options': [],
        'message': None,
        'user_name': "gao's"
}