## cicd使用手册
基于gitlab仓库的项目发布系统
### 安装方法
基本环境:
- Python3.6
- Django2.1+
- Ansible2.7+
- Celery4.2+
- Redis

安装依赖
- pip install -r requirements.txt
- 配置 [redis](./docs/redis.md)

启动服务
- 启动cqcicd
    ```
    sh run.sh start
    ```
- 启动celery
    ```
    sh run.sh celery_start
    ```


### 自动任务
- #### 自动编译和自动发布
当gitlab仓库中有新的push或者合并代码的行为发生时, gitlab hook会将此次事件以post
请求发送给cicd, cicd根据如下流程处理该次任务
1. 拉取代码, 自动编译
2. 自动打包发布到服务器
3. 将本次打包归档, 预留给回滚功能使用

该任务类型仅仅执行.cicd.yml定义的行为<br>
所以,在自动编译和自动发布的过程中, 忽略针对该项目设置的hosts(部署的目标服务器), 默认
使用.cicd.yml文件中定义的hosts :full_moon_with_face:<br>

- #### 自动编译和手动发布
同样接收gitlab post请求和处理任务
1. 拉取代码, 自动编译
2. 打包和归档本次文件
3. 手动点击发布按钮发布到服务器

该任务类型只会执行.cicd.yml文件中的tags为build部分定义行为<br>
点击发布按钮后, cicd会检查是否针对该**项目和分支**设置了hosts, 优先将设置的hosts作为
发布目标服务器, 其次为.cicd.yml中的hosts

### 手动任务
手动任务的作用在于当gitlab的hook发送失败或者cid没有正常接收到gitlab的hook, 此时
执行手动任务, cicd主动执行以下行为
1. 更新本地项目仓库, 从gitlab获取最新数据
2. 获取本次更新的作者以及commit message
3. 指定.cicd.yml中定义的行为

手动任务不存在只执行build任务, 总是会执行完整个.cicd.yml定义的流程(即打包和发布),
在开始一个手动任务前, 可以针对**该任务**配置hosts, 执行发布流程时将该配置的hosts作为
发布服务器<br>
手动任务时, 无论是否针对该项目配置了相应的分支, 都可以进行任务, 如果进行手动任务的
分支之前未添加到项目分支配置文件中, 该手动任务在后期Rollback时无法按照设置的hosts
发布到服务器

### 回滚
当一个新任务处理完成后, 之前的Play按钮(发布按钮)会变成Rollback, 点击即可针对该任务
进行回滚,回滚行为不需要再拉取代码和编译以及打包, 该操作直接查找硬盘上的归档文件, 发布到
目标服务器上<br>
回滚操作只会执行.cicd.yml文件中tags为deploy定义的行为.<br>
无论是自动任务还是手动任务,回滚操作总是会检查是否设置了hosts, 并且优先将设置的hosts作为
发布服务器, 其二再是.cicd.yml文件中的hosts


### 快速开始

#### 添加一个项目
1. step1
+ 访问: http://hostname/tasks/admin/add_project/, 安装提示创建一个项目. 值得注意的是, 如果你设置了目标主机,
该主机将覆盖.cicd.yml中的hosts

2. step2
+ 打开gitlab, 进入项目的settings/integrations, 添加一个Webhook, URL: http://hostname/tasks/te/,
Secret Token: cqcicd项目settings中你配置的SECRET_KEY, 如果没有或者错误的Secret Token, 将不能正常接收Webhook.

3. step3
+ 书写.cicd.yml, 该文件内容为任务执行过程, 语法以及方法参考[Ansible Playbook](https://docs.ansible.com/ansible/latest/user_guide/playbooks.html)
> cqcicd使用ansible-playook执行.cicd.yml,所以, .cicd.yml就是ansible的playbook. 需要注意的是,
每一个play中必须要注明tags, 目前cqcicd只有两个阶段, 即build, deploy. 所以, 在你书写play时, 该play行为是build还是deploy都需要在tags中注明

#### 开始一个任务
+ 自动任务

当你已经配置好一个项目后, 如项目名为: try-api, 分支: master. 后续gitlab仓库中如果该项目的master分支上有push更新,
cqcicd将自动拉取最新的更新, 然后根据.cicd.yml执行任务, 如果你对master设置了自动部署, 将会部署到服务器上, 否则编译后点击`Play`部署
> 注意: 目前针对一个项目, 只能有一个分支可设置自动任务, 如设置了master分支就不能再设置dev分支了. 原因为任务是根据仓库中的.cicd.yml
文件来执行, 无论是master还是dev分支中的.cicd.yml都是一样的, 所以,当你配置了master分支为自动任务时, .cicd.yml中的流程也要根据master
分支的编译和发布流程来书写. 如果你的.cicd.yml文件是根据dev分支的任务流程书写的, 就算你添加了master分支为自动任务, 自动任务执行的还是dev分支的流程.
tip: 你可以对另外一个分支进行手动任务.

+ 手动任务

手动任务针对没有正常接收到Webhook请求或者想手动发布一个分支, 点击`Manual Job` 按钮就可以开始手动任务, 手动任务自开始到结束总是执行整个.cicd.yml文件中定义的流程,
即编译个发布.你可以开始手动任务时指定分支和hosts, 优先级高于.cicd.yml中的设定.

+ 回滚任务

点击`Rollback`按钮即可回滚, 回滚不需要再进行编译, 直接用之前编译的包进行部署. 如果想指定回滚的机器, 可以点击右上角的设置按钮, 设置目标hosts, 该设置是全局生效的,
也就是说设置后, 接下来所有的回滚任务都会发布到设置的目标机器上, 包括新的自动任务发布时也会生效.即该设置的hosts优先级高于.cicd.yml


#### 任务状态指南
![status_buttons](./docs/images/status_buttons.png)

上图实例中, try-api任务下的各种按钮表示(从左至右):
- 任务开始时间
- 任务类型
- 任务对应的分支
- 任务编译状态(编译未完成: 橙色, 编译失败: 红色, 编译完成: 蓝色)
- 任务部署状态(未部署: 橙色, 部署失败: 红色, 已部署: 蓝色)
- 点击可查看任务执行过程
- 任务失败重试按钮(任务执行成功无该按钮)

最右边的`Play`按钮为发布按钮, 当项目的某分支设置为非自动部署时, 任务编译完成后, 需要手动点击该按钮才能发布到对应服务器上,
如果发布失败, 该按钮将变成`Retry`, 成功则变成`Rollback`