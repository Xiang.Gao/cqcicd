function AddGroup(ele) {
    $.ajax({
        url: '/tasks/admin/add_group/',
        type: 'POST',
        data: $('#groupform').serialize(),
        dataType: 'json',
        traditional: true,
        success: function (obj) {
            console.log(obj);
            FillSelected(obj)
        }
    })
}

function FillSelected(obj) {
    let selected = $('<option selected></option>');
    selected.attr('value', obj['group_name']).html(obj['group_name']);
    $('#id_group_name').append(selected)
}

jQuery(document).ready(function() {
    // Switchery
    var elems = Array.prototype.slice.call(document.querySelectorAll('.js-switch'));
    $('.js-switch').each(function() {
        new Switchery($(this)[0], $(this).data());
    });

});


function UpdateProject(ele, project_name) {
    let Branch = $(ele).parent().prev().children().children(':selected').text();
    window.location.href = '/tasks/admin/update_project/' + project_name + '/?branch_name=' + Branch;
}

function UpdateGroup(group_name) {
    window.location.href = '/tasks/admin/update_group/' + group_name
}