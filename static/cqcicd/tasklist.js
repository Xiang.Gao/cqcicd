function Deploy(ele) {
    let text = $(ele).text();
    AddDeployStatus(ele);
    $.ajax({
        url: '/tasks/dt/?tid=' + $(ele).attr('id') + '&action=' + text,
        type: 'GET',
        dataType: 'json',
        traditional: true,
        success: function (obj) {
            console.log(obj);
            if (! obj['status']){
                RemoveDeployStatus(ele, obj['status']);
                alert(obj['err'])
            }else {
                AddDeployTaskId(ele, obj);
                if (obj['action'] === 'Rollback'){
                    GetRollbackStatus(obj)
                }else {
                    GetTaskStatus()
                }
            }
        }
    });
}

function AddDeployTaskId(ele, obj) {
    $(ele).parent().next().children(':last-child').attr('id', obj['taskid'])
}

function AddDeployStatus(ele) {
    $(ele).attr('disabled', true).text("");
    $(ele).append($('<i class="fa fa-spinner Animation"></i>'));
}
function RemoveDeployStatus(ele, status) {
    $(ele).empty();
    if (status){
        $(ele).attr('disabled', false).text('Rollback')
    }else {
        $(ele).attr('disabled', false).text('Retry')
    }
}

function GetTaskStatus() {
    $.ajax({
        url: '/tasks/status/',
        type: 'GET',
        dataType: 'json',
        traditional: true,
        success: function (obj) {
            console.log(obj);
            if(obj['nullid']){
                return
            }
            Object.keys(obj).forEach(function (key) {
                ChangeButtonStatus(key, obj[key]);
                GetTaskStatus()
            });
            if ($.isEmptyObject(obj)){
                GetTaskStatus()
            }
        }
    })
}

function GetRollbackStatus(obj) {
    var retryobj= obj;
    $.ajax({
        url: '/tasks/rollback_status/?taskid=' + obj['taskid'] + '&tid=' + obj['tid'],
        type: 'GET',
        dataType: 'json',
        traditional: true,
        success: function (obj) {
            if ($.isEmptyObject(obj)){
                GetRollbackStatus(retryobj)
            }else {
                RemoveDeployStatus(document.getElementById(obj['tid']), obj['status'])
            }
        }
    })
}

function ChangeButtonStatus(taskid, obj){
    let ele = $('#' + taskid);
    if (obj['status']){
        ele.removeClass('btn-warning').addClass('btn-info')
    }else {
        ele.removeClass('btn-warning').addClass('btn-danger');
        // todo 任务失败添加重试按钮
        AddRetryButton(ele, obj)
    }
    if (obj['type'] === 'deploy'){
        let ele = document.getElementById(obj['tid']);
        RemoveDeployStatus(ele, obj['status']);
        //ChangeButton(obj['checkout_sha'], obj)
    }
    if (obj['auto_deploy']){
        IfAutoDeploy(taskid, obj)
    }
}

function IfAutoDeploy(taskid, obj){
    let ele = $('#' + taskid).next();
    if (obj['status']){
        ele.removeClass('btn-warning').addClass('btn-info')
    }else {
        ele.removeClass('btn-warning').addClass('btn-danger')
    }
    // 如果是自动部署, 修改play按钮状态,
    // todo:当自动部署任务在build阶段发生错误, 此时修改play为retry不合理
    RemoveDeployStatus(document.getElementById(obj['tid']), obj['status'])
}


window.load = GetTaskStatus();


function ManualJob() {
    $.ajax({
        url: '/tasks/manual_task/',
        type: 'POST',
        dataType: 'json',
        data: $('#manualjobform').serialize(),
        traditional: true,
        success: function (obj) {
            if(obj['status']){
                location.reload()
            }else {
                alert(obj['err'])
            }
        }
        })
}

function SetHosts() {
    $.ajax({
        url: '/tasks/set_hosts/',
        type: 'POST',
        dataType: 'json',
        data: $('#sethostsfrom').serialize(),
        traditional: true,
        success: function (obj) {
            if(!obj['status']){
                alert(obj['err'])
            }
        }
        })
}


function AddRetryButton(ele, obj) {
    if (obj['type'] !== 'build'){
        return
    }
    if (ele.parent().children(':contains(Retry)')){
        //修复前端可能生成两个retry按钮的bug
        return
    }
    let retrybutton = $('<button class="btn btn-danger btn-rounded btn-xs" style="width:65px" onclick="Retry(this)"><i class="fa fa-repeat"></i> Retry</button>');
    ele.parent().append(retrybutton)
}


function Retry(ele) {
    let tid = $(ele).parent().prev().children('button').attr('id');
    $.ajax({
        url: '/tasks/retry/?tid=' + tid,
        type: 'GET',
        dataType: 'json',
        traditional: true,
        success: function (obj) {
            if(!obj['status']){
                alert(obj['err'])
            }else {
                location.reload()
            }
        }
    });
    ele.remove();
}


function DeleteTask(tid, bid, did) {
    console.log(tid, bid, did);
    $.ajax({
        url: '/tasks/delete_task/?tid=' + tid +'&buildtaskid=' + bid + '&deploytaskid=' + did,
        type: 'GET',
        dataType: 'json',
        traditional: true,
        success: function (obj) {
            console.log(obj)
        }
    })
}