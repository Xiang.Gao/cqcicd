$(document).ready(
    $.ajax({
        url: '/api/groups/',
        type: 'GET',
        dataType: 'json',
        traditional: true,
        async: false,
        success: function (result) {
            InitSidebar(result)
        }
    })
);

function InitSidebar(result){
    Object.keys(result).forEach(function (key) {
        //console.log(key,result[key]);
        let li_label = $('<li></li>');
        let a_label = $('<a class="waves-effect"></a>');
        let span_label = $('<span class="hide-menu"></span>');
        let inner_span_label = $('<span class="fa arrow"></span>');
        span_label.text(result[key]['GroupName']);
        span_label.append(inner_span_label);
        a_label.append(span_label);
        li_label.append(a_label);
        li_label.append(SecondLevel(result[key]['Project']));
        $('#side-menu li:first').after(li_label)
    })
}

function SecondLevel(Projects) {
    let ul_label = $('<ul class="nav nav-second-level"></ul>');
    Object.keys(Projects).forEach(function (key) {
        let li_label = $('<li></li>');
        let a_label = $('<a></a>');
        let span_label = $('<span class="hide-menu"></span>');
        span_label.text(Projects[key]['name']);
        a_label.attr('href', Projects[key]['task_list_path']);
        a_label.append(span_label);
        li_label.append(a_label);
        ul_label.append(li_label);
    });
    return ul_label
}