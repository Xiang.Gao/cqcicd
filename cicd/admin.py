from django.contrib import admin

# Register your models here.

from cicd import models

admin.site.register(models.GitlabUsers)
admin.site.register(models.Project)
admin.site.register(models.Branches)
admin.site.register(models.ProjectGroups)
admin.site.register(models.Tasks)