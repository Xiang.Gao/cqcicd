from django.db import models
from django.urls import reverse

# Create your models here.


class GitlabUsers(models.Model):
    user_id = models.IntegerField(primary_key=True)
    user_avatar = models.URLField(null=True)
    user_email = models.EmailField(null=True)
    user_name = models.CharField(max_length=32, null=True)
    user_username = models.CharField(max_length=32, null=True)

    def __str__(self):
        return self.user_username


class Project(models.Model):
    id = models.IntegerField(null=True, blank=True)
    name = models.CharField(primary_key=True, max_length=64)
    description = models.TextField(null=True, blank=True)
    ssh_url = models.CharField(max_length=256, null=True, blank=True)
    http_url = models.URLField(null=True, blank=True)
    homepage = models.URLField(null=True, blank=True)
    default_branch = models.CharField(max_length=12, null=True, blank=True)
    group = models.ForeignKey("ProjectGroups", null=True, related_name="Project", on_delete=models.CASCADE)

    def __str__(self):
        return self.name

    def task_list(self):
        return reverse('tasks:tasks-list', args=[self.name])


class Branches(models.Model):
    project_name = models.ForeignKey('Project', on_delete=models.CASCADE, related_name='branches')
    branch_name = models.CharField(max_length=32, null=True)
    branch_domain = models.URLField(null=True, blank=True)
    auto_deploy = models.BooleanField(default=False)
    hosts = models.CharField(max_length=1024, null=True, blank=True)

    def __str__(self):
        return '{} {}'.format(self.project_name, self.branch_name)


class ProjectGroups(models.Model):
    GroupName = models.CharField(max_length=32, null=True)
    GroupWebHook = models.URLField(null=True, blank=True)

    def __str__(self):
        return self.GroupName


class Tasks(models.Model):
    """
    记录每一次task, 自动或者手动
    """
    checkout_sha = models.CharField(max_length=64, null=True)
    event_name = models.CharField(max_length=64, null=True)
    ref = models.CharField(max_length=32, null=True)
    commits_message = models.CharField(max_length=32, null=True)
    commits_url = models.URLField(null=True)
    commits_timestamp = models.CharField(max_length=64, null=True)
    commits_author_name = models.CharField(max_length=32, null=True)
    project = models.ForeignKey('Project', to_field='name', on_delete=models.CASCADE)
    user = models.ForeignKey('GitlabUsers', to_field='user_id', null=True, on_delete=models.DO_NOTHING)
    create_time = models.DateTimeField(auto_now_add=True)
    # task status
    BuildStatus = models.BooleanField(default=False)
    DeployStatus = models.BooleanField(default=False)
    BuildTaskId = models.CharField(max_length=128, null=True)
    DeployTaskId = models.CharField(max_length=128, null=True)
    TaskState = models.CharField(max_length=24, null=True)  # current task state
    TaskType = models.CharField(max_length=24, null=True)  # auto or manual
    DeployHosts = models.CharField(max_length=1024, null=True)

    def __str__(self):
        return "{}:{}".format(self.project.name, self.commits_message)