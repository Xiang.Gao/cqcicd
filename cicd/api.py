# -*- coding:utf-8 -*-
# Author:Gao Xiang

from cicd import models
from rest_framework import viewsets
from cicd.serializers import ProjectGroupsSerializers


class ProjectGroupsViewSet(viewsets.ModelViewSet):
    queryset = models.ProjectGroups.objects.all()
    serializer_class = ProjectGroupsSerializers
