"""CQCICD URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.11/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.conf.urls import url, include
    2. Add a URL to urlpatterns:  url(r'^blog/', include('blog.urls'))
"""
from django.urls import path, re_path
from cicd.views import views, admin

app_name = 'cicd'

urlpatterns = [
    path('te/', views.TasksEntrance.as_view()),
    re_path(r'list/(?P<project>\S+)/', views.TasksList.as_view(), name="tasks-list"),  # (?P<project>\w+)/
    path('dt/', views.DeployTask.as_view(), name='dt'),
    path('status/', views.TaskStatus.as_view(), name='status'),
    path('rollback_status/', views.RollbackStatus.as_view(), name='rollback_status'),
    path('dashboard/', admin.Dashboard.as_view(), name='dashboard'),
    path('manual_task/', views.ManualTask.as_view(), name='manual_task'),
    path('retry/', views.ReTry.as_view(), name='retry'),
    path('set_hosts/', views.SetDeployHosts.as_view(), name='set_hosts'),
    path('manual/', views.ManualDoc.as_view(), name='manual'),
    path('delete_task/', views.DeleteTask.as_view(), name='delete_task'),
    path('console_log/', views.ConsoleLog.as_view(), name='console_log'),

    # cicd admin
    path('admin/add_project/', admin.AddProjects.as_view(), name='add_project'),
    re_path(r'admin/update_project/(?P<project>\S+)/', admin.UpdateProject.as_view(), name='update_project'),
    path('admin/delete_project/', admin.DeleteProject.as_view(), name='delete_project'),
    path('admin/add_group/', admin.AddGroups.as_view(), name='add_group'),
    re_path(r'admin/update_group/(?P<group>.+)/', admin.UpdateGroups.as_view(), name='update_group'),
    path('admin/delete_group/', admin.DeleteGroup.as_view(), name='delete_group'),
    path('admin/add_branch/', admin.AddBranch.as_view(), name='add_branch'),
    path('admin/update_branch/', admin.UpdateBranch.as_view(), name='update_branch'),
    path('admin/delete_branch/', admin.DeleteBranch.as_view(), name='delete_branch')

]
