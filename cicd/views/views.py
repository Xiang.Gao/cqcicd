from CQCICD.settings import DATE_RANGE, SECRET_KEY, TASK_ROOT
from collections import defaultdict
from celery.result import AsyncResult
from cicd import models
from cicd import tasks
from cicd.common.utils import readme
from django.http import HttpResponse, JsonResponse
from django.utils.decorators import method_decorator
from django.views.decorators.csrf import csrf_exempt
from django.contrib.auth.mixins import LoginRequiredMixin
from django.core.cache import cache
from django.views.generic import ListView
from django.views.generic import TemplateView
from django.views import View
from django.utils import timezone
from django.db.models import Q
from cicd import logger
import pytz
import json
import time
import os

# Create your views here.


@method_decorator(csrf_exempt, name='dispatch')
class TasksEntrance(View):
    def post(self, request):
        """
        自动任务入口, 处理gitlab post请求
        :param request:
        :return:
        """
        if request.META.get('HTTP_X_GITLAB_TOKEN') != SECRET_KEY:
            return HttpResponse(status=403)
        data = json.loads(request.body.decode('utf-8'))
        if data['project'].get('name') == 'cqaso-backend':
            self.strange_only(data)
        else:
            context = self.task_context_data(data)
            if not context.get('project'):
                # if project not exist, then return
                logger.error("you must add the project and ref first --> {}, {}".format(
                    data['project'].get('name'), data.get('ref').split('/')[-1]))
                return HttpResponse(status=501)
            obj = models.Tasks.objects.create(**context)
            res = tasks.job.delay(obj.id)
            obj.BuildTaskId = res.id
            obj.save(
                update_fields=['BuildTaskId']
            )
            logger.info(
                "created a new task for --> {}".format(data['project']['name'])
            )
        return HttpResponse(status=200)

    def task_context_data(self, data):
        context = {
            'checkout_sha': data.get('checkout_sha'),
            'event_name': data.get('event_name'),
            'ref': data.get('ref').split("/")[-1],
            'commits_message': data['commits'][0].get('message'),
            'commits_url': data['commits'][0].get('url'),
            'commits_timestamp': data['commits'][0].get('timestamp'),
            'commits_author_name': data['commits'][0]['author'].get('name'),
            'project': self.project_queryset(data['project'], data.get('ref').split('/')[-1]),
            'user': self.user_queryset(data),
            'TaskType': 'auto'
        }
        return context

    @staticmethod
    def project_context_data(data):
        context = {
            'id': data.get('id'),
            'description': data.get('description'),
            'ssh_url': data.get('ssh_url'),
            'http_url': data.get('http_url'),
            'homepage': data.get('homepage'),
            'default_branch': data.get('default_branch')
        }
        return context

    def project_queryset(self, data, ref):
        """
        此处不创建project, 只会对project进行更新
        用户必须先手动创建project, 该project对应的任务才能正常进行
        :param data:
        :param ref:
        :return:
        """
        obj = models.Project.objects.filter(
            name=data.get('name'),
            branches__branch_name=ref
        )
        if not obj:
            logger.error("project --> {} not exist".format(data.get('name')))
            return None
        if not obj.first().id:
            obj.update(**self.project_context_data(data))
            logger.info("update project --> {}".format(data.get('name')))
        return obj.first()

    @staticmethod
    def user_queryset(data):
        context = {
            'user_id': data.get('user_id'),
            'user_avatar': data.get('user_avatar'),
            'user_email': data.get('user_email'),
            'user_name': data.get('user_name'),
            'user_username': data.get('user_username')
        }

        obj, created = models.GitlabUsers.objects.get_or_create(
            defaults=context,
            user_id=data.get('user_id')
        )
        if created:
            logger.info("created a new user --> {}".format(data.get('user_username')))
        return obj

    def strange_only(self, data):
        """
        cqaso backend
        :return:
        """
        project_list = [
            'cqaso-api',
            'cqaso-job',
            'cqaso-crawler',
            'cqaso-consumer'
        ]
        task_context = self.task_context_data(data)  # task_context['project] == None
        project_context = self.project_context_data(data['project'])
        for project in project_list:
            project_context.update({'name': project})
            obj = self.project_queryset(project_context)
            if not obj:
                logger.error("you must add the project first")
            else:
                task_context['project'] = obj
                models.Tasks.objects.create(**task_context)
        res = tasks.job.delay(**task_context)
        models.Tasks.objects.filter(
            checkout_sha=task_context.get('checkout_sha')
        ).update(BuildTaskId=res.id)  # 将会给4个task更新任务id
        logger.info("created a new task for --> cqaso-backend")


class TasksList(LoginRequiredMixin, ListView):
    template_name = "tasks/task_list.html"
    model = models.Tasks

    def get_queryset(self):
        date_start = timezone.datetime.now(tz=timezone.utc) - timezone.timedelta(days=DATE_RANGE)
        date_end = timezone.datetime.now(tz=timezone.utc)
        queryset = super().get_queryset().filter(
            project__name=self.kwargs.get("project"),  # the project parameter pass from url
            create_time__gt=date_start,
            create_time__lte=date_end
        ).order_by('-id')
        return queryset

    def get_context_data(self, **kwargs):
        context = {
            "date_range": self.get_date_range(),
            "date_tasks": self.sort_queryset(),
            "project_group": self.get_project_group(),
            "project_name": self.kwargs.get('project'),
            "branches": self.get_project_branches()
        }
        kwargs.update(context)
        return super().get_context_data(**kwargs)

    @staticmethod
    def get_date_range():
        """
        以当前时间为基础, 获取 DATE_RANGE定义的时间范围详细日期
        :return:
        """
        date_list = []
        for day in range(DATE_RANGE-1, -1, -1):
            date_list.append(
                (timezone.datetime.today() - timezone.timedelta(days=day)).strftime("%Y-%m-%d")
            )
        return date_list

    def sort_queryset(self):
        _sorted = {}
        for date in self.get_date_range():
            _sorted[date] = self.object_list.filter(
                create_time__contains=date
            )
        return _sorted

    def get_project_group(self):
        if self.object_list.first():
            return self.object_list.first().project.group.GroupName
        return models.Project.objects.get(
            name=self.kwargs.get('project')
        ).group.GroupName

    def get_project_branches(self):
        if self.object_list.first():
            return self.object_list.first().project.branches.all()
        return models.Branches.objects.filter(project_name=self.kwargs.get('project')).all()


class DeployTask(LoginRequiredMixin, View):
    # todo: 如果部署任务类型是rollback, 则返回新的task id, 前端根据该task id获取部署状态
    def get(self, request):
        tid = request.GET.get('tid')
        try:
            obj = models.Tasks.objects.get(id=tid)
        except models.Tasks.DoesNotExist:
            return JsonResponse({})
        if not obj.BuildStatus:
            return JsonResponse(
                {
                    'status': False,
                    'err': '打包未完成'
                }
            )
        res = tasks.deploy.delay(tid)
        obj.DeployTaskId = res.id
        obj.save(update_fields=['DeployTaskId'])
        return JsonResponse({
            'status': True, 'tid': tid, 'taskid': res.id, 'action': request.GET.get('action')
        })


class TaskStatus(LoginRequiredMixin, View):

    def get(self, request):
        """
        从数据库中取出未完成的任务id, 10s内持续获取这些id的状态, 期间检测到完成的任务则返回该任务状态, 未有任务完成持续10s直到返回空,
        将期间完成的任务id移出未完成列表
        :param request:
        :return:
        """

        task_id_list = self.load_task_id()
        if not task_id_list:
            logger.info('return nullid, because task_id_list={}'.format(task_id_list))
            return JsonResponse(
                {
                    'nullid': True
                }
            )
        for t in range(10):
            context = {}
            for task_id in task_id_list:
                if AsyncResult(task_id).ready() and not cache.get(task_id):
                    context.update(
                        {task_id: task_id_list[task_id]}
                    )
                    context[task_id].update(
                        {'status': AsyncResult(task_id).get()}
                    )
            if context:
                print(context)
                for key in context.keys():
                    cache.set(key, True, 60 * 60 * 24 * DATE_RANGE)
                return JsonResponse(context)
            time.sleep(1)
        return JsonResponse({})

    @staticmethod
    def load_task_id():
        """
        从数据库中取出未完成的任务的id
        :return:
        """
        date_range = timezone.datetime.now(tz=pytz.UTC) - timezone.timedelta(days=DATE_RANGE)
        query_set = models.Tasks.objects.filter(
            Q(create_time__gt=date_range),
            # Q(BuildStatus=False) | Q(DeployStatus=False),
            # ~Q(TaskState__contains='failed')
        )
        ids = defaultdict(dict)
        for query in query_set:
            if query.BuildTaskId:
                ids[query.BuildTaskId].update({
                    'type': 'build',
                    'tid': query.id,
                    # 1, 如果在分支中找不到该分支, 则该任务为手动任务, 手动任务都是自动部署的
                    # 2, 如果进行的手动任务找到了对应分支, 同样设置auto_deploy为自动
                    'auto_deploy': query.project.branches.filter(branch_name=query.ref).first().auto_deploy \
                            if query.project.branches.filter(branch_name=query.ref) and query.TaskType == 'auto' else True
                })
            if query.DeployTaskId:
                ids[query.DeployTaskId].update({
                    'type': 'deploy',
                    'tid': query.id,
                })
        return ids


class RollbackStatus(LoginRequiredMixin, View):
    def get(self, request):
        tid = request.GET.get('tid')
        taskid = request.GET.get('taskid')
        for t in range(10):
            context = {}
            if AsyncResult(taskid).ready():
                context = {
                    'tid': tid,
                    'taskid': taskid,
                    'type': 'Rollback',
                    'status': AsyncResult(taskid).get()
                }
            if context:
                return JsonResponse(context)
            time.sleep(1)
        return JsonResponse({})


class ManualTask(LoginRequiredMixin, View):
    def post(self, request):
        branch_name = request.POST.get('branch_name')
        if not branch_name:
            return JsonResponse({'status': False, 'err': 'branch is null'})
        obj = models.Tasks.objects.create(**self.task_context_data(request.POST))
        res = tasks.manual_job.delay(obj.id)
        obj.BuildTaskId = res.id
        obj.save(update_fields=['BuildTaskId'])
        return JsonResponse({
            'status': True,
            'tid': obj.id,
            'taskid': res.id
        })

    @staticmethod
    def task_context_data(data):
        context = {
            'project': models.Project.objects.get(name=data.get('project_name')),
            'ref': data.get('branch_name').strip(),
            'TaskType': 'manual',
            'DeployHosts': data.get('hosts')
        }
        return context


class ReTry(LoginRequiredMixin, View):
    def get(self, request):
        obj = models.Tasks.objects.get(id=request.GET.get('tid'))
        if obj.TaskType == 'manual':
            res = tasks.manual_job.delay(tid=obj.id)
            # 手动任务总是希望拉取最新的代码, 所以checkout_sha设为None后即会拉取新代码
            obj.checkout_sha = None
        else:
            res = tasks.job.delay(tid=obj.id)
        obj.TaskState = None
        obj.BuildTaskId = res.id
        obj.save()
        return JsonResponse({
            'status': True
        })


class SetDeployHosts(LoginRequiredMixin, View):
    def post(self, request):
        if not request.POST.get('branch_name'):
            return JsonResponse({
                'status': False,
                'err': '没有分支, 先添加分支信息'
            })
        obj = models.Branches.objects.filter(
            project_name=request.POST.get('project_name'),
            branch_name=request.POST.get('branch_name')
        ).update(hosts=request.POST.get('hosts'))
        return JsonResponse({
            'status': True
        })


class ManualDoc(LoginRequiredMixin, TemplateView):
    template_name = 'manual.html'

    def get_context_data(self, **kwargs):
        context = {
            'readme': repr(readme())
        }
        kwargs.update(context)
        return super().get_context_data(**kwargs)


class DeleteTask(LoginRequiredMixin, View):
    def get(self, request):
        task_id = request.GET.get('buildtaskid') if request.GET.get('buildtaskid') else request.GET.get('deploytaskid')
        if AsyncResult(task_id).ready():
            models.Tasks.objects.filter(BuildTaskId=task_id).delete()
            return JsonResponse({})
        AsyncResult(task_id).revoke(terminate=True, signal='SIGKILL')
        models.Tasks.objects.filter(BuildTaskId=task_id).delete()
        return JsonResponse({})


class ConsoleLog(LoginRequiredMixin, TemplateView):
    template_name = 'tasks/console.html'

    def get_context_data(self, **kwargs):
        obj = models.Tasks.objects.get(id=self.request.GET.get('tid'))
        context = {
            'tid': self.request.GET.get('tid'),
            'project_name': obj.project.name,
            'commits': obj.commits_message,
            'offset': 0,
        }
        kwargs.update(**context)
        return super().get_context_data(**kwargs)

    def post(self, request):
        jobs_file = os.path.join(TASK_ROOT, 'jobs', request.POST.get('tid'))
        fd = open(jobs_file, 'r', encoding='utf-8')
        fd.seek(int(request.POST.get('offset')))
        logs = fd.read()
        offset = fd.tell()
        fd.close()
        EOF = False
        task_state = models.Tasks.objects.get(id=request.POST.get('tid')).TaskState
        if task_state == 'failed' or task_state == 'deploy succeed':
            EOF = True
        return JsonResponse({'offset': offset, 'logs': logs, 'EOF': EOF})
