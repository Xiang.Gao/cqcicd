# -*- coding:utf-8 -*-
# Author:Gao Xiang
from django.contrib.auth.mixins import LoginRequiredMixin
from django.views.generic import ListView, TemplateView, UpdateView
from django.http import JsonResponse
from django.views import View
from django.shortcuts import redirect, reverse
from cicd import models


class Dashboard(LoginRequiredMixin, ListView):
    template_name = 'admin/dashboard.html'
    model = models.Project

    def get_context_data(self, **kwargs):
        group_model = models.ProjectGroups.objects.all()
        context = {
            'projects': self.object_list,
            'project_count': self.object_list.count(),
            'task_count': models.Tasks.objects.count(),
            'group_count': group_model.count(),
            'groups': group_model,
            'branches': models.Branches.objects.all(),
        }
        kwargs.update(**context)
        return super().get_context_data(**kwargs)


class AddProjects(LoginRequiredMixin, ListView):
    template_name = 'admin/add_project.html'
    model = models.ProjectGroups

    def get_context_data(self, **kwargs):
        context = {
            'groups': self.object_list,
        }
        kwargs.update(**context)
        return super().get_context_data(**kwargs)

    def post(self, request):
        context = {
            'name': request.POST.get('project_name'),
            'ssh_url': request.POST.get('ssh_url'),
            'group': self.model.objects.get(GroupName=request.POST.get('group_name')),
        }
        obj, created = models.Project.objects.get_or_create(
            defaults=context,
            name=request.POST.get('project_name')
        )
        if not created:
            return JsonResponse({
                'status': False,
                'err': 'the project you submit is already in database'
            })
        if request.POST.get('branch_name'):
            branch_context = {
                'project_name': obj,
                'branch_name': request.POST.get('branch_name'),
                'branch_domain': request.POST.get('branch_domain'),
                'auto_deploy': True if request.POST.get('auto_deploy') == 'on' else False,
                'hosts': request.POST.get('hosts')
            }
            obj, created = models.Branches.objects.get_or_create(
                defaults=branch_context,
                project_name=obj,
                branch_name=request.POST.get('branch_name')
            )
            if not created:
                return JsonResponse({
                    'status': False,
                    'err': 'the project and branch you submit is already in database'
                })
        return redirect(
            reverse(
                'tasks:tasks-list',
                kwargs={'project': request.POST.get('project_name')}
            )
        )


class UpdateProject(LoginRequiredMixin, TemplateView):
    template_name = 'admin/update_project.html'

    def get_context_data(self, **kwargs):
        context = {
            'project_name': self.kwargs.get('project'),
            'project': models.Project.objects.get(name=self.kwargs.get('project')),
            'branch': models.Branches.objects.filter(project_name=self.kwargs.get('project'),
                                                     branch_name=self.request.GET.get('branch_name')
                                                     ).first(),
            'groups': models.ProjectGroups.objects.all()
        }
        kwargs.update(**context)
        return super().get_context_data(**kwargs)

    def post(self, request, **kwargs):
        context = {
            'name': request.POST.get('project_name'),
            'ssh_url': request.POST.get('ssh_url'),
            'group': models.ProjectGroups.objects.get(GroupName=request.POST.get('group_name')),
        }
        obj, created = models.Project.objects.update_or_create(
            name=request.POST.get('project_name'),
            defaults=context
        )
        if request.POST.get('branch_name'):
            branch_context = {
                'project_name': obj,
                'branch_name': request.POST.get('branch_name'),
                'branch_domain': request.POST.get('branch_domain'),
                'auto_deploy': True if request.POST.get('auto_deploy') == 'on' else False,
                'hosts': request.POST.get('hosts')
            }
            obj = models.Branches.objects.update_or_create(
                project_name=request.POST.get('project_name'),
                branch_name=request.POST.get('branch_name'),
                defaults=branch_context
            )
        return redirect(
            reverse('tasks:update_project',
                    kwargs={'project': request.POST.get('project_name')}
                    ) + '?branch_name={}'.format(request.POST.get('branch_name'))
        )


class DeleteProject(LoginRequiredMixin, View):
    def get(self, request):
        obj = models.Project.objects.filter(
            name=request.GET.get('project_name')
        ).delete()
        return redirect(reverse("tasks:dashboard"))


class AddGroups(LoginRequiredMixin, TemplateView):
    template_name = 'admin/add_group.html'

    def post(self, request):
        obj, created = models.ProjectGroups.objects.update_or_create(
            defaults={
                'GroupName': request.POST.get('GroupName'),
                'GroupWebHook': request.POST.get('GroupWebHook')
            },
            GroupName=request.POST.get('GroupName')
        )
        if not request.is_ajax():
            return redirect(reverse('tasks:dashboard'))
        return JsonResponse({
            'status': True,
            'group_name': obj.GroupName
        })


class UpdateGroups(LoginRequiredMixin, TemplateView):
    template_name = 'admin/update_group.html'

    def get_context_data(self, **kwargs):
        context = {
            'group': models.ProjectGroups.objects.get(GroupName=self.kwargs.get('group'))
        }
        kwargs.update(**context)
        return super().get_context_data(**kwargs)

    def post(self, request, **kwargs):
        obj = models.ProjectGroups.objects.filter(
            GroupName=kwargs.get('group')
        ).update(
            GroupName=request.POST.get('GroupName'),
            GroupWebHook=request.POST.get('GroupWebHook')
        )
        return redirect(
            reverse('tasks:update_group', kwargs={'group': request.POST.get('GroupName')})
        )


class DeleteGroup(LoginRequiredMixin, View):
    def get(self, request):
        obj = models.ProjectGroups.objects.filter(
            GroupName=request.GET.get('group_name')
        ).delete()
        return redirect(reverse("tasks:dashboard"))


class AddBranch(LoginRequiredMixin, TemplateView):
    template_name = 'admin/add_branch.html'

    def get_context_data(self, **kwargs):
        context = {
            'projects': models.Project.objects.all()
        }
        kwargs.update(**context)
        return super().get_context_data(**kwargs)

    def post(self, request):
        project_obj = models.Project.objects.filter(name=request.POST.get('project_name'))
        if not project_obj:
            return JsonResponse({
                'status': False,
                'err': 'invalid data'
            })
        context = {
            'project_name': project_obj.first(),
            'branch_name': request.POST.get('branch_name'),
            'branch_domain': request.POST.get('branch_domain'),
            'hosts': request.POST.get('hosts'),
            'auto_deploy': True if request.POST.get('auto_deploy') == 'on' else False,
        }
        obj, created = models.Branches.objects.get_or_create(
            defaults=context,
            branch_name=request.POST.get('branch_name'),
            project_name=request.POST.get('project_name'),
        )
        if not created:
            return JsonResponse({
                'status': False,
                'err': 'the branch and project you submit is already in used'
            })
        return redirect(reverse('tasks:dashboard'))


class UpdateBranch(LoginRequiredMixin, TemplateView):
    template_name = 'admin/update_branch.html'

    def get_context_data(self, **kwargs):
        context = {
            'branch': models.Branches.objects.get(
                project_name=self.request.GET.get('project_name'),
                branch_name=self.request.GET.get('branch_name')
            )
        }
        kwargs.update(**context)
        return super().get_context_data(**kwargs)

    def post(self, request):
        context = {
            'branch_domain': request.POST.get('branch_domain'),
            'hosts': request.POST.get('hosts'),
            'auto_deploy': True if request.POST.get('auto_deploy') == 'on' else False
        }
        obj = models.Branches.objects.filter(
            project_name=request.POST.get('project_name'),
            branch_name=request.POST.get('branch_name'),
        ).update(**context)
        return redirect(
            reverse('tasks:update_branch') + '?project_name={}&branch_name={}'.format(
                request.POST.get('project_name'),
                request.POST.get('branch_name')
            )
        )


class DeleteBranch(LoginRequiredMixin, View):
    def get(self, request):
        obj = models.Branches.objects.filter(
            branch_name=request.GET.get('branch_name'),
            project_name=request.GET.get('project_name')
        ).delete()
        return redirect(reverse("tasks:dashboard"))
