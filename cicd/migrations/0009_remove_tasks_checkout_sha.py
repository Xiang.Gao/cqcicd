# Generated by Django 2.1.5 on 2019-01-30 03:37

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('cicd', '0008_auto_20190130_1058'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='tasks',
            name='checkout_sha',
        ),
    ]
