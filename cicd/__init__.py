from CQCICD.settings import LOGGING
import logging.config

logging.config.dictConfig(LOGGING)
logger = logging.getLogger('cqcicd.cicd')

default_app_config = 'cicd.apps.CicdConfig'