# -*- coding:utf-8 -*-
# Author:Gao Xiang

from django import template
from django.utils import timezone

register = template.Library()


@register.simple_tag
def load_task(date_tasks, date):
    return date_tasks.get(date)


@register.simple_tag
def converse_time(date):
    try:
        return (date + timezone.timedelta(hours=8)).strftime("%H:%M")
    except ValueError:
        return None