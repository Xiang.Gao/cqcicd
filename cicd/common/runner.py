# -*- coding:utf-8 -*-
# Author:Gao Xiang

import os
import json
import shutil
from CQCICD.settings import TASK_ROOT
from collections import namedtuple
from ansible.executor.playbook_executor import PlaybookExecutor
from ansible.parsing.dataloader import DataLoader
from ansible.vars.manager import VariableManager
from ansible.inventory.manager import InventoryManager
from ansible.playbook.play import Play
from ansible.executor.task_queue_manager import TaskQueueManager
from ansible.plugins.callback import CallbackBase
import ansible.constants as C
from cicd.common.utils import rsa_local, load_hosts
from cicd import models
from cicd import logger
from celery import current_task
from django.db.models import Q


class ResultCallback(CallbackBase):
    """A sample callback plugin used for performing an action as results come in

    If you want to collect all results into a single object for processing at
    the end of the execution, look into utilizing the ``json`` callback plugin
    or writing your own custom callback plugin
    """
    def v2_runner_on_ok(self, result, **kwargs):
        """Print a json representation of the result

        This method could store the result in an instance attribute for retrieval later
        """
        host = result._host
        logger.info('task completed: hostname: {}, taskname: {}'.format(host.name, result.task_name))
        fd = self.get_fd()
        fd.write(result._result.get('stdout') if result._result.get('stdout') else result._result.get('cmd', '-'))
        fd.write('\n')
        fd.write('task completed: hostname: {}, taskname: {}\n'.format(host.name, result.task_name))
        fd.close()
        # print(json.dumps(result._result.get("stdout"), indent=4))

    def v2_runner_on_failed(self, result, **kwargs):
        """Print a json representation of the result

        This method could store the result in an instance attribute for retrieval later
        """
        host = result._host
        logger.error('task failed: hostname: {}, taskname: {}'.format(host.name, result.task_name))
        fd = self.get_fd()
        fd.write(result._result.get('stdout') if result._result.get('stdout') else result._result.get('cmd', '-'))
        fd.write('\n')
        fd.write('task failed: hostname: {}, taskname: {}\n'.format(host.name, result.task_name))
        fd.close()
        # print(json.dumps({host.name: result._result['stdout_lines']}, indent=4))

    def v2_runner_on_unreachable(self, result, **kwargs):
        host = result._host
        logger.error('task unreachable: hostname: {}, taskname: {}, \nresult: {}'.format(host.name, result.task_name, result._result))
        fd = self.get_fd()
        fd.write(result._result.get('msg'))
        fd.write('task unreachable: hostname: {}, taskname: {}\n'.format(host.name, result.task_name))
        fd.close()
        # print(json.dumps({host.name: result._result}, indent=4))

    def v2_playbook_on_task_start(self, task, is_conditional):
        logger.info('task started: {}'.format(task.name))
        fd = self.get_fd()
        fd.write('task started: {}'.format(task.name) + '\n')
        fd.close()

    @staticmethod
    def get_fd():
        jobs_path = os.path.join(TASK_ROOT, 'jobs')
        if not os.path.exists(jobs_path):
            os.mkdir(jobs_path)
        tid = models.Tasks.objects.filter(
            Q(BuildTaskId=current_task.request.id) | Q(DeployTaskId=current_task.request.id)
        ).first().id
        jobs_file = os.path.join(jobs_path, str(tid))
        fd = open(jobs_file, 'a', encoding='utf-8')
        return fd


class Runner:
    def __init__(self, tags='all'):
        self.Options = namedtuple(
            'Options', [
                'listtags',
                'listtasks',
                'listhosts',
                'syntax',
                'connection',
                'module_path',
                'forks',
                'become',
                'become_method',
                'become_user',
                'check',
                'diff',
                'private_key_file',
                'ssh_common_args',
                'tags'
            ]
        )
        self.options = self.Options(
            listtags=False,
            listtasks=False,
            listhosts=False,
            syntax=False,
            connection=None,
            module_path=None,
            forks=10,
            become=None,
            become_method=None,
            become_user=None,
            check=False,
            diff=False,
            private_key_file=rsa_local(),
            ssh_common_args="-o StrictHostKeyChecking=no",
            tags=[tags]
        )
        self.loader = DataLoader()
        self.passwords = dict()
        self.results_callback = ResultCallback()

    def playbook_executor(self, playbook_path):
        inventory = InventoryManager(loader=self.loader, sources='{},'.format(load_hosts(playbook_path)))
        variable_manager = VariableManager(loader=self.loader, inventory=inventory)
        play = PlaybookExecutor(
            playbooks=[playbook_path],
            variable_manager=variable_manager,
            inventory=inventory,
            loader=self.loader,
            options=self.options,
            passwords=self.passwords

        )

        play._tqm._stdout_callback = self.results_callback
        result = play.run()
        # if task execute no err, then return 0 else 2
        return result

    def play_executor(self, **kwargs):
        inventory = InventoryManager(loader=self.loader, sources='{},'.format(kwargs.get('hosts')))
        variable_manager = VariableManager(loader=self.loader, inventory=inventory)
        # create data structure that represents our play, including tasks,
        # this is basically what our YAML loader does internally.
        play_source = kwargs

        # Create play object, playbook objects use .load instead of init or new methods,
        # this will also automatically create the task objects from the info provided in play_source
        play = Play().load(play_source, variable_manager=variable_manager, loader=self.loader)

        # Run it - instantiate task queue manager, which takes care of forking and
        # setting up all objects to iterate over host list and tasks
        tqm = None
        try:
            tqm = TaskQueueManager(
                      inventory=inventory,
                      variable_manager=variable_manager,
                      loader=self.loader,
                      options=self.options,
                      passwords=self.passwords,
                      # Use our custom callback instead of the ``default`` callback plugin, which prints to stdout
                      stdout_callback=self.results_callback,
                  )
            result = tqm.run(play)  # most interesting data for a play is actually sent to the callback's methods
            return result
        finally:
            # we always need to cleanup child procs and the structres we use to communicate with them
            if tqm is not None:
                tqm.cleanup()

            # Remove ansible tmpdir
            shutil.rmtree(C.DEFAULT_LOCAL_TMP, True)
