# -*- coding:utf-8 -*-
# Author:Gao Xiang

from CQCICD import settings
from cicd import logger
import subprocess
import yaml
import os


# todo: 编写单元测试


def project_path(name):
    if settings.PROJECT_DIR.startswith('/'):
        path = os.path.join(settings.PROJECT_DIR, name)
    else:
        path = os.path.join(settings.TASK_ROOT, settings.PROJECT_DIR, name)
    return path


def playbook_path(name):
    path = os.path.join(project_path(name), '.cicd.yml')
    if not os.path.exists(path):
        logger.error('current project directory missing .cicd.yml --> {}'.format(name))
        return
    return path


def rsa_local():
    """
    获取私钥位置, 优先获取用户自定义, 不存在即获取当前用户家目录默认私钥
    :return:
    """
    if settings.PRIVATE_KEY:
        if os.path.exists(settings.PRIVATE_KEY):
            return settings.PRIVATE_KEY
        logger.error("you have assigned a private key, but {} not exist or not access".format(settings.PRIVATE_KEY))
    id_rsa = os.path.join(os.environ.get('HOME'), '.ssh', 'id_rsa')
    if not os.path.exists(id_rsa):
        logger.error("can't find a private key for current user, you may need create it first")
    return id_rsa


def yaml_editor(obj):
    yaml_file = playbook_path(obj.project.name)
    if not os.path.exists(yaml_file):
        logger.error('current project directory missing .cicd.yml --> {}'.format(obj.project.name))
        return

    fd = open(yaml_file, 'r', encoding='utf-8')
    try:
        json_load = yaml.load(fd)
    except yaml.YAMLError as e:
        logger.error("{} not a valid yaml or syntax error")
        return
    playlist = []
    for play in json_load:
        if play.get('tags') == 'deploy':
            # 优先获取对应分支的hosts, 然后再是playbook自身的hosts
            if obj.project.branches.filter(branch_name=obj.ref).first().hosts:
                play['hosts'] = obj.project.branches.filter(branch_name=obj.ref).first().hosts
            # 在tags为deploy的play中找到unarchive位置, 修改其src值
            for module in play['tasks']:
                if module.get('unarchive'):
                    play['tasks'][play['tasks'].index(module)]['unarchive']['src'] = \
                        os.path.join(module['unarchive']['src'].rsplit('/', 1)[0], obj.checkout_sha)
                    break
                    # return play
            # just support serial=1
            if play.get('serial'):
                for host in play['hosts'].split(','):
                    play['hosts'] = host
                    playlist.append(play.copy())
                return playlist
            playlist.append(play)
            return playlist
    logger.error("can't find 'deploy' tags in playbook --> {}".format(yaml_file))
    fd.close()
    return


def update_yaml(obj):
    file = os.path.join(settings.BASE_DIR, 'cicd/config/update.yml')
    fd = open(file, 'r', encoding='utf-8')
    json_load = yaml.load(fd)
    fd.close()
    json_load[0]['tasks'][0]['git']['dest'] = project_path(obj.project.name)
    json_load[0]['tasks'][0]['git']['key_file'] = rsa_local()
    json_load[0]['tasks'][0]['git']['repo'] = obj.project.ssh_url
    json_load[0]['tasks'][0]['git']['version'] = obj.checkout_sha if obj.checkout_sha else obj.ref
    return json_load[0]


def load_hosts(path):
    hosts = []
    fd = open(path, 'r', encoding='utf-8')
    for play in yaml.load(fd):
        hosts.append(play.get('hosts'))
    fd.close()
    return ",".join(hosts)


def load_filepath(name):
    """
    从给定的playbook中读取archive模块的dest路径,
    如果不存在读取unarchive的src路径, 两个模块的这两个值是相同的.
    :return:
    """
    yaml_file = playbook_path(name)
    fd = open(yaml_file, 'r', encoding='utf-8')
    for play in yaml.load(fd):
        for module in play['tasks']:
            if module.get('archive'):
                return module['archive']['dest']
            if module.get('unarchive'):
                return module['unarchive']['src']
    fd.close()
    logger.error("can't find 'archive' or 'unarchive' module in playbook --> {}".format(yaml_file))
    return


def check_tags(path):
    """
    检查playbook中是否设置了tags, 强制要求playbook中必须设置tags
    :param path:
    :return:
    """
    fd = open(path, 'r', encoding='utf-8')
    tags = []
    for play in yaml.load(fd):
        tags.append(play.get('tags', None))
    fd.close()
    return all(tags)


def load_gitlog(obj):
    output = subprocess.check_output(['git', '-C', project_path(obj.project.name), 'show', '--summary'])
    try:
        msg = output.decode('utf-8')
    except UnicodeEncodeError:
        import sys
        import io
        sys.stdout = io.TextIOWrapper(sys.stdout.buffer, encoding='utf-8')
        msg = output.decode('utf-8')
    msg = [x for x in msg.split('\n') if x]
    obj.checkout_sha = msg[0].split(' ')[1]
    obj.commits_author_name = msg[1].split(' ')[1] if msg[1].split(' ')[0] == 'Author:' else msg[2].split(' ')[1]
    obj.commits_message = msg[-1].strip()
    obj.save()
    return True


def manual_yaml(obj):
    """
    判断是否设置了发布的目标机器, 如果没有设置, 则默认.cicd.yml文件中配置的hosts
    :param obj:
    :return:
    """
    yaml_file = playbook_path(obj.project.name)
    if not yaml_file:
        return
    fd = open(yaml_file, 'r', encoding='utf-8')
    json_load = yaml.load(fd)
    fd.close()
    # if not obj.DeployHosts:
    #     return json_load
    playlist = []
    for play in json_load:
        if play.get('tags') == 'build':
            # 保持play中其他的git拉取的分支和当前任务分支一致
            for task in play['tasks']:
                if task.get('git'):
                    task['git']['version'] = obj.ref
                    break
            playlist.append(play.copy())
        if play.get('tags') == 'deploy':
            if obj.DeployHosts:
                play['hosts'] = obj.DeployHosts
            # just support serial=1
            if play.get('serial'):
                for host in play['hosts'].split(','):
                    play['hosts'] = host
                    playlist.append(play.copy())
            else:
                playlist.append(play.copy())
    return playlist


def readme():
    with open(os.path.join(settings.BASE_DIR, 'README.md'), 'r', encoding='utf-8') as f:
        return f.read()