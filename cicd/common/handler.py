# -*- coding:utf-8 -*-
# Author:Gao Xiang


from ansible.parsing.dataloader import DataLoader
from ansible.vars.manager import VariableManager
from ansible.inventory.manager import InventoryManager
from ansible.playbook.play import Play
from ansible.executor.task_queue_manager import TaskQueueManager
from ansible.executor.playbook_executor import PlaybookExecutor
from ansible.plugins.callback import CallbackBase
from collections import namedtuple
from collections import defaultdict
from CQCICD.settings import PRIVATE_KEY, BASE_DIR
from CQCICD import django_env
from cicd import logger
import ansible.constants as C
import shutil
import os


DEBUG = django_env()

if DEBUG:
    hosts_path = os.path.join(BASE_DIR, 'cicd', 'config', 'hosts')
else:
    hosts_path = os.path.join(os.path.sep, 'etc', 'cicd', 'config', 'hosts')


class ResultCallback(CallbackBase):
    """A sample callback plugin used for performing an action as results come in

    If you want to collect all results into a single object for processing at
    the end of the execution, look into utilizing the ``json`` callback plugin
    or writing your own custom callback plugin
    """

    result = defaultdict(list)

    def v2_runner_on_ok(self, result, **kwargs):
        """Print a json representation of the result

        This method could store the result in an instance attribute for retrieval later
        """
        host = result._host
        # print(json.dumps({host.name: result._result}, indent=4))
        # logger.info('hostname: {}, result: {}, taskname: {}'.format(host.name, result._result, result.task_name))
        logger.info('hostname: {}, taskname: {}'.format(host.name, result.task_name))
        self.result['ok'].append((host.name, result.task_name))

    def v2_runner_on_failed(self, result, **kwargs):
        host = result._host
        # print(json.dumps({host.name: result._result}, indent=4))
        logger.error('hostname: {}\n taskname: {}\n result: {}'.format(host.name, result._result, result.task_name))
        self.result['failed'].append((host.name, result.task_name))

    def v2_runner_on_unreachable(self, result, **kwargs):
        host = result._host
        # print(json.dumps({host.name: result._result}, indent=4))
        logger.error('hostname: {}\n taskname: {}\n result: {}'.format(host.name, result._result, result.task_name))
        self.result['unreachable'].append((host.name, result.task_name))


class AnsibleApi:

    def __init__(self):
        Options = namedtuple(
            'Options',
            [
                'tags',
                'listtags',
                'listtasks',
                'listhosts',
                'syntax',
                'connection',
                'module_path',
                'forks',
                'remote_user',
                'private_key_file',
                'timeout',
                'ssh_common_args',
                'ssh_extra_args',
                'sftp_extra_args',
                'scp_extra_args',
                'become',
                'become_method',
                'become_user',
                'check',
                'extra_vars',
                'passwords',
                'diff'
            ]
        )
        self.options = Options(
            tags=['all'],
            listtags=False,
            listtasks=False,
            listhosts=False,
            syntax=False,
            timeout=60,
            connection='local',
            module_path='',
            forks=10,
            remote_user='root',
            private_key_file=PRIVATE_KEY,
            ssh_common_args="-o StrictHostKeyChecking=no",
            ssh_extra_args="",
            sftp_extra_args="",
            scp_extra_args="",
            become=False,
            become_method=False,
            become_user=False,
            extra_vars=[],
            check=False,
            passwords=None,
            diff=False,
        )

    def runplaybook(self, playbook_path):

        # initialize needed objects
        # Takes care of finding and reading yaml, json and ini files
        loader = DataLoader()
        passwords = dict()

        # Instantiate our ResultCallback for handling results as they come in.
        # Ansible expects this to be one of its main display outlets
        results_callback = ResultCallback()

        # create inventory, use path to host config file as source or hosts in a comma separated string
        inventory = InventoryManager(loader=loader, sources=hosts_path)

        # variable manager takes care of merging all the different sources to give you
        #  a unifed view of variables available in each context
        variable_manager = VariableManager(loader=loader, inventory=inventory)

        # Create play object, playbook objects use .load instead of init or new methods,
        # this will also automatically create the task objects from the info provided in play_source
        play = PlaybookExecutor(
            playbooks=[playbook_path],
            inventory=inventory,
            loader=loader,
            options=self.options,
            passwords=passwords,
            variable_manager=variable_manager
        )

        play._tqm._stdout_callback = results_callback
        result = play.run()

        # print(result)
        # 防止两次执行playbook结果的混淆
        results = results_callback.result.copy()
        results_callback.result.clear()
        return results


if __name__ == '__main__':
    # playbook()
    a = AnsibleApi()
    a.runplaybook(playbook_path='/usr/local/etc/ansible/test.yml')