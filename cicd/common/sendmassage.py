# -*- coding:utf-8 -*-
# Author:Gao Xiang
#
# import os
# import django
# os.environ['DJANGO_SETTINGS_MODULE'] = 'CQCICD.settings'
# django.setup()


from CQCICD import django_env
from cicd import models
import requests

DEBUG = django_env()


def dingding(obj, auto_deploy):
    if not auto_deploy:
        return
    msg = {
            "msgtype": "markdown",
            "markdown": {
                "title": "发布通知",
                "text": "#### 发布通知 \n > project: {0} \n\n > status: {1} \n\n > branch: {2} \n\n  > pusher: {3} \n\n > commits: {4} \n\n > domain: [{5}]({6})\n"
            }
    }
    try:
        domain = obj.project.branches.get(branch_name=obj.ref).branch_domain
    except models.Branches.DoesNotExist:
        domain = None

    msg['markdown']['text'] = msg['markdown']['text'].format(
        obj.project.name,
        obj.TaskState,
        obj.ref,
        obj.commits_author_name,
        obj.commits_message,
        domain,
        domain
    )

    requests.post(url=obj.project.group.GroupWebHook, json=msg)
