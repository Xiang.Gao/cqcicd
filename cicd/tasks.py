# -*- coding:utf-8 -*-
# Author:Gao Xiang

from __future__ import absolute_import, unicode_literals

# import os
# import django
# os.environ['DJANGO_SETTINGS_MODULE'] = 'CQCICD.settings'
# django.setup()

from cicd.common import utils
from cicd.common.sendmassage import dingding
from cicd.common.runner import Runner
from cicd import models
from celery import shared_task
from celery.signals import task_postrun
from . import logger
import os


@shared_task
def job(tid):
    """
    如果任务设置了自动部署, 则默认将任务发布到.cicd.yml文件描述的hosts中, 如果没有设置自动部署,
    只会打包, 点击发布按钮后, 具体的发布规则见deploy任务
    :param tid:
    :return:
    """
    obj = models.Tasks.objects.get(id=tid)
    # 任务开始前, 对每个project进行update, 以获取最新的数据
    if update_project(obj) != 0:
        logger.error('update project failed --> {}'.format(obj.project.name))
        obj.TaskState = 'failed'
        obj.save(update_fields=['TaskState'])
        return False
    logger.info('update project completed --> {}'.format(obj.project.name))

    # 每个project对应多个分支, 分支记录了该分支是否自动部署
    # 获取当前项目对应的分支是否设置了自动部署, 如果未获取到相关配置, 则默认打包, 不部署
    try:
        auto_deploy = obj.project.branches.get(branch_name=obj.ref).auto_deploy
    except models.Branches.DoesNotExist:
        logger.info('current project and branch not '
                    'configure branches settings --> {}, {}'.format(obj.project.name, obj.ref)
                    )
        auto_deploy = False
    if auto_deploy:
        task_runner = Runner()
    else:
        task_runner = Runner(tags='build')

    playbook_path = utils.playbook_path(obj.project.name)
    if not utils.check_tags(playbook_path):
        obj.TaskState = 'failed'
        obj.save(update_fields=['TaskState'])
        logger.error("missing tags in playbook --> {}".format(playbook_path))
        return False
    if not playbook_path:
        obj.TaskState = 'failed'
        obj.save(update_fields=['TaskState'])
        return False
    if task_runner.playbook_executor(playbook_path) != 0:
        logger.error('an error occurred while executing playbook, {}'.format(playbook_path))
        obj.TaskState = 'failed'
        obj.save(update_fields=['TaskState'])
        return False
    if auto_deploy:
        obj.BuildStatus = True
        obj.DeployStatus = True
        obj.TaskState = 'deploy succeed'
    else:
        obj.BuildStatus = True
        obj.DeployStatus = False
        obj.TaskState = 'build succeed'
    obj.save(update_fields=['BuildStatus', 'DeployStatus', 'TaskState'])
    dingding(obj, auto_deploy)
    return True


def update_project(obj):
    task_runner = Runner()
    logger.info('update project --> {}'.format(obj.project.name))
    return task_runner.play_executor(**utils.update_yaml(obj))


@shared_task
def deploy(tid):
    """
    将打包好的文件发布到服务器, 检查该任务的hosts, 不为空则发布到该hosts, 否则, 检查
    该项目和分支对应的hosts, 不为空则作为发布目标, 都为空则将.cicd.yml文件中的hosts作为
    发布目标
    :param tid:
    :return:
    """
    obj = models.Tasks.objects.get(id=tid)
    task_runner = Runner(tags='deploy')
    playlist = utils.yaml_editor(obj)
    if not playlist:
        return False
    for play in playlist:
        if task_runner.play_executor(**play) != 0:
            obj.TaskState = 'failed'
            obj.save(update_fields=['TaskState'])
            dingding(obj, auto_deploy=True)
            return False
    obj.DeployStatus = True
    obj.TaskState = 'deploy succeed'
    obj.save(update_fields=['DeployStatus', 'TaskState'])
    dingding(obj, auto_deploy=True)
    return True


@task_postrun.connect()
def rename(sender=None, **kwargs):
    """
    执行完job即修改包名
    当project对象设置了自动部署, 那么执行完job后即部署完成, 需要修改包名,
    当project没有自动部署, 执行job只做了build动作, 那么需要在deploy中修改src路径中的包名
    :param sender:
    :param kwargs:
    :return:
    """
    if sender.name == 'cicd.tasks.deploy' or not kwargs.get('retval'):
        return
    try:
        tid = kwargs.get('args')[0]
    except IndexError:
        tid = kwargs['kwargs'].get('tid')
    obj = models.Tasks.objects.get(id=tid)
    src = utils.load_filepath(obj.project.name)
    if not src:
        return
    dest = os.path.join(src.rsplit('/', 1)[0], obj.checkout_sha)
    try:
        os.rename(src, dest)
    except FileNotFoundError as e:
        logger.error(e)
        return False
    return True


@shared_task
def manual_job(tid):
    """
    手动任务不存在只build, 总是会跑完整个流程
    :param tid:
    :return:
    """
    obj = models.Tasks.objects.get(id=tid)
    if update_project(obj) != 0:
        logger.error('update project failed --> {}'.format(obj.project.name))
        obj.TaskState = 'failed'
        obj.save(update_fields=['TaskState'])
        return False
    if not utils.load_gitlog(obj):
        logger.error("can't read git log from repository --> {}".format(obj.objects.name))
        return False
    task_runner = Runner()
    playlist = utils.manual_yaml(obj)
    if not playlist:
        obj.TaskState = 'failed'
        obj.save(update_fields=['TaskState'])
        return False
    for play in playlist:
        if task_runner.play_executor(**play) != 0:
            obj.TaskState = 'failed'
            obj.save(update_fields=['TaskState'])
            return False
    obj.BuildStatus = True
    obj.DeployStatus = True
    obj.TaskState = 'deploy succeed'
    obj.save(
        update_fields=['BuildStatus', 'DeployStatus', 'TaskState']
    )
    dingding(obj, auto_deploy=True)
    return True


