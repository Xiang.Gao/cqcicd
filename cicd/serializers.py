# -*- coding:utf-8 -*-
# Author:Gao Xiang

from rest_framework import serializers
from cicd.models import ProjectGroups, Project


class ProjectSerializers(serializers.HyperlinkedModelSerializer):
    task_list_path = serializers.ReadOnlyField(source='task_list')

    class Meta:
        model = Project
        fields = ('name', 'http_url', 'task_list_path')


class ProjectGroupsSerializers(serializers.HyperlinkedModelSerializer):
    Project = ProjectSerializers(many=True)

    class Meta:
        model = ProjectGroups
        fields = ('GroupName', 'Project', 'GroupWebHook')
