from django.apps import AppConfig
from CQCICD import settings
import os


class CicdConfig(AppConfig):
    name = 'cicd'

    def ready(self):
        """
        应用启动后初始化目录
        :return:
        """
        if not hasattr(settings, 'TASK_ROOT'):
            path = '/var/lib/cicd'
            setattr(settings, 'TASK_ROOT', path)
            os.makedirs(path, exist_ok=True)
        else:
            if not settings.TASK_ROOT.startswith('/'):
                settings.TASK_ROOT = os.path.join(settings.BASE_DIR, settings.TASK_ROOT)
            os.makedirs(settings.TASK_ROOT, exist_ok=True)
        if not hasattr(settings, 'PROJECT_DIR'):
            setattr(settings, 'PROJECT_DIR', 'projects')
            absolute_path = os.path.join(settings.TASK_ROOT, 'projects')
            os.makedirs(absolute_path, exist_ok=True)
        else:
            if not settings.PROJECT_DIR.startswith('/'):
                settings.PROJECT_DIR = os.path.join(settings.TASK_ROOT, settings.PROJECT_DIR)
            os.makedirs(settings.PROJECT_DIR, exist_ok=True)
        if not hasattr(settings, 'RELEASE_DIR'):
            setattr(settings, 'RELEASE_DIR', 'release')
            absolute_path = os.path.join(settings.RELEASE_DIR, 'release')
            os.makedirs(absolute_path, exist_ok=True)
        else:
            if not settings.RELEASE_DIR.startswith('/'):
                settings.RELEASE_DIR = os.path.join(settings.TASK_ROOT, settings.RELEASE_DIR)
            os.makedirs(settings.RELEASE_DIR, exist_ok=True)

        if not os.path.exists('/var/lib/cicd'):
            os.makedirs('/var/lib/cicd', exist_ok=True)